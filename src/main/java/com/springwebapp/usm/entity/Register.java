package com.springwebapp.usm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.springwebapp.usm.constants.AppConstants;

@Entity
@Table(name=AppConstants.REGISTER_INFO)
public class Register implements Serializable
 {
	@Id
	@GenericGenerator(name="auto" , strategy="increment")
	@GeneratedValue(generator="auto")
	@Column(name="id")
	private Long id;
	@Column(name="name")
	private String name;
	@Column(name="email")
	private String email;
	
	@Column(name="contactNumber")
	private String contactNumber;
	@Column(name="country")
	private  String country;
	@Column(name="password")
	private String password;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getContactNumber() {
	return contactNumber;
}

public void setContactNumber(String contactNumber) {
	this.contactNumber = contactNumber;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
	return "Register [id=" + id + ", name=" + name + ", email=" + email + ", contactNumber=" + contactNumber
			+ ", country=" + country + ", password=" + password + "]";
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

}
