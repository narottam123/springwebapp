package com.springwebapp.usm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.springwebapp.usm.constants.AppConstants;
import com.springwebapp.usm.entity.LoginDto;
import com.springwebapp.usm.entity.Register;
import com.springwebapp.usm.service.AuthorizationService;

@RequestMapping(value=AppConstants.FORWARD_SLASH)
@Controller
public class AuthorizationController {
	
	@Autowired
	private AuthorizationService authorizationService;
	
	public AuthorizationController()
	{
		System.out.println(this.getClass().getSimpleName()+"class object created");
	}
	
	@RequestMapping(value=AppConstants.SAVE_REGISTER_DETILS)
	public ModelAndView saveRegisterDetails(Register register)
	{
		System.out.println("exicuting saveRegisterDetails () of authorizationController class");
		authorizationService.saveRegisterDetails(register);
		return new ModelAndView("login.jsp");
	}
	
	@PostMapping(value=AppConstants.LOGIN_INFO)
	//@RequestMapping(value=AppConstants.LOGIN_INFO)
	public ModelAndView login(LoginDto loginDto)
	{
		Register register= authorizationService.login(loginDto);
		if(register!=null) {
			return new ModelAndView("home.jsp");
	}
		else {
			return new ModelAndView("login.jsp");
		}
	}
        
}
