package com.springwebapp.usm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springwebapp.usm.constants.AppConstants;
import com.springwebapp.usm.entity.Register;
import com.springwebapp.usm.service.AuthorizationService;
import com.springwebapp.usm.util.AppResponse;

//@Controller
//@ResponseBody
@RestController
@RequestMapping(value=AppConstants.FORWARD_SLASH)
public class CommonController {
	@Autowired
	private AuthorizationService authorizationService;

	@GetMapping(value=AppConstants.FINDALL)
	//public @ResponseBody List<Register> findAll()
	public AppResponse findAll()
	{
		
  return authorizationService.findAll();
   }
	
	@PostMapping(value=AppConstants.SAVEJSON)
	public void saveRegisterDetails( @RequestBody Register register)
	{
		authorizationService.saveRegisterDetails(register); 
	}
	@GetMapping(value=AppConstants.FIND_MOBILE_NUMBER)
	public Register findByContactNumber(@PathVariable("mobile") String contactNumber) {
		
		return  authorizationService.findByContactNumber(contactNumber);
	}
		@GetMapping(value=AppConstants.FIND_BY_NAME)
		public AppResponse findByName( @RequestHeader("name") String name)
		{
			return authorizationService.findByName(name);
		}
		
	  
}
