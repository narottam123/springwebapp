package com.springwebapp.usm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springwebapp.usm.entity.LoginDto;
import com.springwebapp.usm.entity.Register;
import com.springwebapp.usm.repository.AuthorizationRepository;
import com.springwebapp.usm.util.AppResponse;

@Service
public class AuthorizationService {
	
	@Autowired
	private AuthorizationRepository authorizationRepository;
	
	public AuthorizationService()
	{
		System.out.println(this.getClass().getSimpleName()+"class object created");
	}

	
	public void saveRegisterDetails(Register register)
	{
		authorizationRepository.saveRegisterDetails(register);
	}


	public Register login(LoginDto loginDto) {
		
		return authorizationRepository.getByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword());
	}
	public AppResponse findAll()
	{
		AppResponse appResponse=null;
		try {
			List<Register> registerList = authorizationRepository.findAll();
			appResponse=new AppResponse("SUCCESS","200", registerList,null);
		}
		catch(Exception e)
		{
			appResponse=new AppResponse("FAILURE", "500", null, e.getMessage());
		}
		//return authorizationRepository.findAll();
		return appResponse;
	
	}
	public Register findByContactNumber(String contactNumber)
	{
		return authorizationRepository.findByContactNumber(contactNumber);
	}
	public AppResponse findByName(String name)
	{
		AppResponse appResponse=null;
		try {
		Register registerList = authorizationRepository.findByName(name);
			appResponse=new AppResponse("SUCCESS","200", registerList,null);
		}
		catch(Exception e)
		{
			appResponse=new AppResponse("FAILURE", "500", null, e.getMessage());
		}

		return appResponse;	

}
	}
