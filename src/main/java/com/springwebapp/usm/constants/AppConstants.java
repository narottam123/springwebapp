
package com.springwebapp.usm.constants;

public interface AppConstants {

	public static String REGISTER_INFO="springwebapp";
	public static String SAVE_REGISTER_DETILS="/saveRegisterDetails";
	public static String FORWARD_SLASH="/";
	public static String LOGIN_INFO="login";
	public static String FINDALL="/findAll";
	public static String SAVEJSON="/save";
	public static String FIND_MOBILE_NUMBER="/find_by_contact_number/{mobile}";
	public static String FIND_BY_NAME="/findName";
}
