package com.springwebapp.usm.repository;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.springwebapp.usm.entity.LoginDto;
import com.springwebapp.usm.entity.Register;
@Repository
public class AuthorizationRepository {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AuthorizationRepository() {
		System.out.println(this.getClass().getSimpleName() +"class object created");
	}
	
	public void saveRegisterDetails(Register register)
	{
	System.out.println("executing SaveRegisterDetails() of AuthoriationRepository"+register);
	                                // dont need to create object of configuration because localsession factory bean 
	                             // internaly creating the singlton class
	
	Session session = sessionFactory.openSession();
	Transaction transaction = session.beginTransaction();
		session.save(register);
		transaction.commit();
	}
	public Register getByEmailAndPassword(String email, String password )
	{
		String qry="from Register where email=:naro and password=:pwd" ;
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(qry);
		query.setParameter("naro", email);
		query.setParameter("pwd", password);
		Register reg= (Register) query.uniqueResult();
		System.out.println(reg);
		//return (Register) query.uniqueResult();
		return reg;
		
	}
	public List<Register> findAll()
	{
		String qry=" from Register";
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(qry);
		return  query.list();
	}
	
	public Register findByContactNumber(String contactNumber)
	{
		Session session = sessionFactory.openSession();
		String hql="from Register where contactNumber=:cn";
		Query query = session.createQuery(hql);
		query.setParameter("cn", contactNumber);
		return (Register) query.uniqueResult();
		
	}
	public Register findByName(String name)
	{
		Session session = sessionFactory.openSession();
		String hql="from Register where name=:n";
		Query query = session.createQuery(hql);
           query.setParameter("n", name);
           return (Register) query.uniqueResult();
	}

}
